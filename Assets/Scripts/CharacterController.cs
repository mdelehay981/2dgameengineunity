﻿using UnityEngine;

public class CharacterController : MonoBehaviour
{
    protected Rigidbody2D rb;
    protected SpriteRenderer sr;
    private float rayCastLengthCheck = 0.05f;
    private float width;
    private float height;
    public float speed = 1f;
    public float accel = 1f;

    protected virtual void Awake() {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        width = GetComponent<Collider2D>().bounds.extents.x + 0.01f;
        height = GetComponent<Collider2D>().bounds.extents.y + 0.02f;
    }

    public bool IsCharacterOnGround() { 
        bool groundCheck1 = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - height), -Vector2.up, rayCastLengthCheck); 
        bool groundCheck2 = Physics2D.Raycast(new Vector2(transform.position.x + (width - 0.2f), transform.position.y - height), -Vector2.up, rayCastLengthCheck);
        bool groundCheck3 = Physics2D.Raycast(new Vector2(transform.position.x - (width - 0.2f), transform.position.y - height), -Vector2.up, rayCastLengthCheck);
        if (groundCheck1 || groundCheck2 || groundCheck3) {
            return true;
        } else {
            return false;
        } 
    }

    // Update is called once per frame
    void Update() {
        
    }
}
