﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer), typeof(Rigidbody2D), typeof(Animator))]
public class HeroController : CharacterController {
    private Vector2 input;
    private Animator animator;
    public bool isJumping;
    private float jumpDuration;
    private AudioSource audioSource;
    public float jumpSpeed = 8f;
    public float jumpDurationThreshold = 0.25f;
    public float airAccel = 3f;
    public float jump = 14f;
    public AudioClip runClip;
    public AudioClip jumpClip;
    public AudioClip slideClip;
    public Sprite hitSprite;    // hero sprite when it's hit by enemy 

    protected override void Awake() {
        base.Awake(); 
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    void PlayAudioClip(AudioClip clip)
    {
        if (audioSource != null && clip != null) {
            if (!audioSource.isPlaying) audioSource.PlayOneShot(clip);
        }
    }
    
    // Update is called once per frame
    void Update() {
        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Jump");
        animator.SetFloat("Speed", Mathf.Abs(input.x));
        if (input.x > 0f) {
            sr.flipX = false;
        } else if (input.x < 0f) {
            sr.flipX = true;
        }

        if (input.y >= 1f) {
            jumpDuration += Time.deltaTime;
            animator.SetBool("IsJumping", true);
        } else {
            isJumping = false;
            animator.SetBool("IsJumping", false);
            jumpDuration = 0f;
        }

        if (IsCharacterOnGround() && !isJumping) {
            if (input.y > 0f) {
                isJumping = true;
                PlayAudioClip(jumpClip);
            }            
            if (input.x < 0f || input.x > 0f) {
                PlayAudioClip(runClip);
            }
        }

        if (jumpDuration > jumpDurationThreshold) input.y = 0f;
    }

    void FixedUpdate() {
        var acceleration = 0f;
        if (IsCharacterOnGround()) {
            acceleration = accel;
        } else {
            acceleration = airAccel;
        }
        var xVelocity = 0f;
        if (IsCharacterOnGround() && input.x == 0) {
            xVelocity = 0f;
        } else {
            xVelocity = rb.velocity.x;
        }
        var yVelocity = 0f;
        if (IsCharacterOnGround() && input.y == 1) {
            yVelocity = jump;
        } else {
            yVelocity = rb.velocity.y;
        }
        rb.AddForce(new Vector2(((input.x * speed) - rb.velocity.x) * acceleration, 0));
        rb.velocity = new Vector2(xVelocity, yVelocity);

        if (isJumping && jumpDuration < jumpDurationThreshold) {
            rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
        }
    }
}
