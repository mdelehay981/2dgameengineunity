﻿using UnityEngine;

public class EnemyController : CharacterController {
    private int walkingDirectionSign = 1;  // walking direction: right (+1) or left (-1)    
    private float walkedDistanceInUnit = 0f;    // currently walked distance  
    private bool keepOnMoving = true; 
    public float maximumDistanceToWalkInUnit = 5f;  // maximum distance to walk before u-turning  
    public Sprite hitSprite;    // hero sprite when it's hit by enemy 
    public GameObject playerDeathPrefab; 

    protected override void Awake() {
        base.Awake();
    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.transform.tag == "Player") {
            //var ps = GetComponent<ParticleSystem>();
            //var psMain = ps.main;
            //psMain.startSize = 1; 
            Instantiate(playerDeathPrefab, coll.contacts[0].point, Quaternion.identity);
            sr.sprite = hitSprite;
            keepOnMoving = false; // enemy must stop moving    
            Destroy(coll.gameObject);
            GameManager.instance.RestartLevel(1.25f); 
        }
    }

    private void Update() {
        // Flip sprite in following of rigidbody velocity 
        if (rb.velocity.x > 0f) {
            sr.flipX = false;
        } else if (rb.velocity.x < 0f) {
            sr.flipX = true;
        }
    }

    void FixedUpdate() {
        if (!IsCharacterOnGround() || !keepOnMoving) {
            return; 
        } else {
            // If enemy has reached maximum distance to walk then walking direction is changed and walked distance is reset 
            if (walkedDistanceInUnit < maximumDistanceToWalkInUnit) {
                walkedDistanceInUnit += Time.fixedDeltaTime * speed;
            } else { 
                walkedDistanceInUnit = 0f; 
                walkingDirectionSign = -walkingDirectionSign;  
            }
            rb.AddForce(new Vector2(((walkingDirectionSign * speed) - rb.velocity.x) * accel, 0));
            //Debug.Log("EnemyController: FixedUpdate: Time.fixedDeltaTime = " + Time.fixedDeltaTime 
            //    + ", walkingDirectionSign = " + walkingDirectionSign
            //    + ", rb.velocity.x = " + rb.velocity.x
            //    + ", rb.position.x = " + rb.position.x
            //    + ", walkedDistanceInUnit = " + walkedDistanceInUnit); 
        }
    }
}
